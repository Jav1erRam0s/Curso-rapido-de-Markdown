# Titulo

<!-- Titulo y Subtitulos -->

## Subtitulo Nivel 1

### Subtitulo Nivel 2

#### Subtitulo Nivel 3

##### Subtitulo Nivel 4

###### Subtitulo Nivel 5

# Indice

<!-- Tenemos 3 formas para hacerlo: *, + y - -->

- Origenes y formacion futbolistica
- Trayectoria en clubes
  - Primeros pasos en boca
  - Consagracion en Napoli
  - Sevilla

* Con la seleccion Argentina
  - Primeras convocaciones
  - Campeon mundial en Mexico 1986

- 1. Estadisticas
  - 1.1. Clubes
  - 1.2. Selecciones
  - 1.3. Entrenador
  - 1.4. Resumen estadistico
  - 1.5. Hat-tricks

# Enlaces

<!-- Enlaces -->

Para mas informacion de 'El Diego' ir [aqui](https://es.wikipedia.org/wiki/Diego_Maradona "Custom title").

Para mas informacion de 'El Che' ir [aqui](https://es.wikipedia.org/wiki/Che_Guevara "El Che").

# Citas

<!-- Citas -->

Notas de Diario de motocicleta

> "No es este el relato de hazañas impresionantes, es un trozo de dos vidas tomadas
> en un momento en que cursaron juntas un determinado trecho, con identidad de aspiraciones
> y conjunción de en sueños. Fue nuestra visión demasiado estrecha, demasiado parcial,
> demasiado apresurada, fueron nuestras conclusiones demasiado rígidas, tal vez, pero ese
> vagar sin rumbo por nuestra mayúscula América me ha cambiado más de lo que creí. Yo, ya
> no soy yo, por lo menos no soy el mismo yo interior.
>
> Ernesto 'Che' Guevara, 1951

# Resaltar texto de una cita

> Note: `--capt-add=SYS-ADMIN` is required for PDF rendering.

# Insertar codigo

<!-- Insertar codigo -->

```sh
npm init
npm i
node app
```

```javascript
function myf2() {
  setTimeout(function () {
    document.getElementById("boton").style.backgroundColor = "blue";
  }, 1000);
}
```

```html
<html>
  <head>
    <title>Mi primer codigo HTML</title>
  </head>
  <body>
    <h1>Hola Mundo!</h1>
  </body>
</html>
```

```java
public class Main
{

	public static void main(String[] args)
    {
		System.out.println("Hola Mundo!");
    }

}
```

# Tabla

<!-- Insertar tablas -->

| Columna 1 | Columna 2 |
| --------- | --------- |
| A         | B         |
| C         | D         |

# Fuentes

<!-- Cursiva Negrita y Cursiva negrita -->

_cursiva_

_cursiva_

**negrita**

**negrita**

**_cursiva y negrita_**

**_cursiva y negrita_**

~~tachado~~

# Separadores

<!-- Tenemos 3 formas para hacerlo -->

---

---

---

# Importar imagenes

<!-- Importar imagenes -->

<!-- Local -->

![Diego Armando Maradona](img/maradona.jpg "El Diego")

<!-- Remota -->

![Ernesto 'Che' Guevara](https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/CheHigh.jpg/220px-CheHigh.jpg "El Che")
[![Ernesto 'Che' Guevara](https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/CheHigh.jpg/220px-CheHigh.jpg "CHE")](https://es.wikipedia.org/wiki/Che_Guevara)

<!-- Align img local center -->

<p align="center" >
  <img width="200" heigth="200" src="img/maradona.jpg">
</p>

<!-- Imagen tipo .gif remote -->

![Magia-Maradonian](https://c.tenor.com/RfQptsmCcuoAAAAC/diego-maradona-maradona.gif)

<!-- Align img remote center -->

<p align="center">
  <img src="https://c.tenor.com/RfQptsmCcuoAAAAC/diego-maradona-maradona.gif" alt="Magia-Maradoniana" />
</p>

# Checkbox

- [ ] task 1 (no terminada)
- [x] task 2 (terminada)
- [x] task 3 (terminada)
- [ ] task 4 (no terminada)

# Emojis

<!-- ref.: https://gist.github.com/rxaviers/7360908 -->

:high_brightness: :guitar: :sunglasses:
