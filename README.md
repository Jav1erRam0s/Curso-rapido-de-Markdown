# Curso rapido de Markdown

Curso rapido de Markdown.

* Titulo y Subtitulos
* Indice
* Enlaces
* Citas
* Resaltar texto de una cita
* Insertar codigo
* Tabla
* Fuentes
    * Cursiva
    * Negrita
    * Cursiva y Negrita
    * Tachado
* Separadores
* Importar imagenes
    * Local
    * Remota
* Checkbox
* Emojis